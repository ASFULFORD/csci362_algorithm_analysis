#include <string>
#include <iostream>
#include <fstream>
#include <time.h>
#include <cstdlib>
#include "my_sort_functions.h"
#define FILE "output.csv"
#define RUNS 100
using namespace std;
//Each prototype is pretty self explanatory.
//I decided to do it this way so that each
//function call could have it's own new array.
//The premise behind this whole attempt is to run 
//each algorithm with n number of nodes 100 times
//and get an average good average time. I will 
//comment on function, and the rest are exactly
//the same, only a few minor variable changes.
void unsorted_insertion100();
void unsorted_insertion500();
void unsorted_insertion1000();
void unsorted_insertion2000();
void unsorted_insertion5000();
void unsorted_insertion10000();

void sorted_insertion100();
void sorted_insertion500();
void sorted_insertion1000();
void sorted_insertion2000();
void sorted_insertion5000();
void sorted_insertion10000();

void unsorted_quick100();
void unsorted_quick500();
void unsorted_quick1000();
void unsorted_quick2000();
void unsorted_quick5000();
void unsorted_quick10000();

void sorted_quick100();
void sorted_quick500();
void sorted_quick1000();
void sorted_quick2000();
void sorted_quick5000();
void sorted_quick10000();

void unsorted_heap100();
void unsorted_heap500();
void unsorted_heap1000();
void unsorted_heap2000();
void unsorted_heap5000();
void unsorted_heap10000();

void sorted_heap100();
void sorted_heap500();
void sorted_heap1000();
void sorted_heap2000();
void sorted_heap5000();
void sorted_heap10000();

void unsorted_merge100();
void unsorted_merge500();
void unsorted_merge1000();
void unsorted_merge2000();
void unsorted_merge5000();
void unsorted_merge10000();

void sorted_merge100();
void sorted_merge500();
void sorted_merge1000();
void sorted_merge2000();
void sorted_merge5000();
void sorted_merge10000();

ofstream myfile;
ofstream random100;
void main()
{
	
	myfile.open(FILE);
	random100.open("random100.txt");
	myfile << ",Sorted,,,,,,Unsorted\nN Elements,100,500,1000,2000,5000,10000,100,500,1000,2000,5000,10000";
	myfile << "\nInsertion";
	
	cout<<"Sorted insertion 100."<<endl;
	sorted_insertion100();
	cout<<"Sorted insertion 500."<<endl;
	sorted_insertion500();
	cout<<"Sorted insertion 1000."<<endl;
	sorted_insertion1000();
	cout<<"Sorted insertion 2000."<<endl;
	sorted_insertion2000();
	cout<<"Sorted insertion 5000."<<endl;
	sorted_insertion5000();
	cout<<"Sorted insertion 10000."<<endl;
	sorted_insertion10000();
	
	cout<<"Unsorted insertion 100."<<endl;
	unsorted_insertion100();
	cout<<"Unsorted insertion 500."<<endl;
	unsorted_insertion500();
	cout<<"Unsorted insertion 1000."<<endl;
	unsorted_insertion1000();
	cout<<"Unsorted insertion 2000."<<endl;
	unsorted_insertion2000();
	cout<<"Unsorted insertion 5000."<<endl;
	unsorted_insertion5000();
	cout<<"Unsorted insertion 10000."<<endl;
	unsorted_insertion10000();

	myfile << "\nQuick";
		
	cout<<"Sorted quick 100."<<endl;
	sorted_quick100();
	cout<<"Sorted quick 500."<<endl;
	sorted_quick500();
	cout<<"Sorted quick 1000."<<endl;
	sorted_quick1000();
	cout<<"Sorted quick 2000."<<endl;
	sorted_quick2000();
	cout<<"Sorted quick 5000."<<endl;
	sorted_quick5000();
	cout<<"Sorted quick 10000."<<endl;
	sorted_quick10000();
	
	cout<<"Unsorted quick 100."<<endl;
	unsorted_quick100();
	cout<<"Unsorted quick 500."<<endl;
	unsorted_quick500();
	cout<<"Unsorted quick 1000."<<endl;
	unsorted_quick1000();
	cout<<"Unsorted quick 2000."<<endl;
	unsorted_quick2000();
	cout<<"Unsorted quick 5000."<<endl;
	unsorted_quick5000();
	cout<<"Unsorted quick 10000."<<endl;
	unsorted_quick10000();

	myfile << "\nHeap";

	cout<<"Sorted heap 100."<<endl;
	sorted_heap100();
	cout<<"Sorted heap 500."<<endl;
	sorted_heap500();
	cout<<"Sorted heap 1000."<<endl;
	sorted_heap1000();
	cout<<"Sorted heap 2000."<<endl;
	sorted_heap2000();
	cout<<"Sorted heap 5000."<<endl;
	sorted_heap5000();
	cout<<"Sorted heap 10000."<<endl;
	sorted_heap10000();
	
	cout<<"Unsorted heap 100."<<endl;
	unsorted_heap100();
	cout<<"Unsorted heap 500."<<endl;
	unsorted_heap500();
	cout<<"Unsorted heap 1000."<<endl;
	unsorted_heap1000();
	cout<<"Unsorted heap 2000."<<endl;
	unsorted_heap2000();
	cout<<"Unsorted heap 5000."<<endl;
	unsorted_heap5000();
	cout<<"Unsorted heap 10000."<<endl;
	unsorted_heap10000();

	myfile <<"\nMerge";

	cout<<"Sorted merge 100."<<endl;
	sorted_merge100();
	cout<<"Sorted merge 500."<<endl;
	sorted_merge500();
	cout<<"Sorted merge 1000."<<endl;
	sorted_merge1000();
	cout<<"Sorted merge 2000."<<endl;
	sorted_merge2000();
	cout<<"Sorted merge 5000."<<endl;
	sorted_merge5000();
	cout<<"Sorted merge 10000."<<endl;
	sorted_merge10000();
	
	cout<<"Unsorted merge 100."<<endl;
	unsorted_merge100();
	cout<<"Unsorted merge 500."<<endl;
	unsorted_merge500();
	cout<<"Unsorted merge 1000."<<endl;
	unsorted_merge1000();
	cout<<"Unsorted merge 2000."<<endl;
	unsorted_merge2000();
	cout<<"Unsorted merge 5000."<<endl;
	unsorted_merge5000();
	cout<<"Unsorted merge 10000."<<endl;
	unsorted_merge10000();

	random100.close();
	myfile.close();
}
void unsorted_insertion100()
{
	//seed the random num generator
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size100 = 100, count = 0, numrandom100 = 0;
	int times[RUNS];
	int n100[100];
	//create the random numbers in an array
	//and output those numbers to a file
	for(int i = 0; i < size100; i++)
	{
		 numrandom100 = rand() % 20001;
		 n100[i] = numrandom100;
		 random100 << numrandom100 << " ";
	}
	//this loop runs the sort algorith 100 times and outputs the time
	//creating a new random array each time and putting the run times
	//into an array for later.
	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n100, size100);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		if(count >= RUNS)
			break;

		for(int i = 0; i < size100; i++)
			n100[i] = rand() % 20001;

		count++;
	}
	//this loop adds the times together
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];
	//this finds the average
	average = sum / size100;
	//and outputs to file
	myfile << "," << average;
}
void unsorted_insertion500()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size500 = 500, count = 0;
	int times[RUNS];
	int n500[500];
	
	for(int i = 0; i < size500; i++)
		n500[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n500, size500);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size500; i++)
			n500[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size500;
	myfile << "," << average;
}
void unsorted_insertion1000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size1000 = 1000, count = 0;
	int times[RUNS];
	int n1000[1000];
	
	for(int i = 0; i < size1000; i++)
		n1000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n1000, size1000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size1000; i++)
			n1000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size1000;
	myfile << "," << average;
}
void unsorted_insertion2000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size2000 = 2000, count = 0;
	int times[RUNS];
	int n2000[2000];
	
	for(int i = 0; i < size2000; i++)
		n2000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n2000, size2000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size2000; i++)
			n2000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size2000;
	myfile << "," << average;
}
void unsorted_insertion5000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size5000 = 5000, count = 0;
	int times[RUNS];
	int n5000[5000];
	
	for(int i = 0; i < size5000; i++)
		n5000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n5000, size5000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size5000; i++)
			n5000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size5000;
	myfile << "," << average;
}
void unsorted_insertion10000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size10000 = 10000, count = 0;
	int times[RUNS];
	int n10000[10000];
	
	for(int i = 0; i < size10000; i++)
		n10000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n10000, size10000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size10000; i++)
			n10000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size10000;
	myfile << "," << average;
}
void sorted_insertion100()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size100 = 100, count = 0;
	int times[RUNS];
	int n100[100];
	
	for(int i = 0; i < size100; i++)
		n100[i] = rand() % 20001;
	
	insertion_sort(n100, size100);

	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n100, size100);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size100;
	myfile << "," << average;
}
void sorted_insertion500()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size500 = 500, count = 0;
	int times[RUNS];
	int n500[500];
	
	for(int i = 0; i < size500; i++)
		n500[i] = rand() % 20001;
	
	insertion_sort(n500, size500);

	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n500, size500);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size500;
	myfile << "," << average;
}
void sorted_insertion1000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size1000 = 1000, count = 0;
	int times[RUNS];
	int n1000[1000];
	
	for(int i = 0; i < size1000; i++)
		n1000[i] = rand() % 20001;
	
	insertion_sort(n1000, size1000);

	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n1000, size1000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size1000;
	myfile << "," << average;
}
void sorted_insertion2000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size2000 = 2000, count = 0;
	int times[RUNS];
	int n2000[2000];
	
	for(int i = 0; i < size2000; i++)
		n2000[i] = rand() % 20001;
	
	insertion_sort(n2000, size2000);

	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n2000, size2000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size2000;
	myfile << "," << average;
}
void sorted_insertion5000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size5000 = 5000, count = 0;
	int times[RUNS];
	int n5000[5000];
	
	for(int i = 0; i < size5000; i++)
		n5000[i] = rand() % 20001;
	
	insertion_sort(n5000, size5000);

	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n5000, size5000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size5000;
	myfile << "," << average;
}
void sorted_insertion10000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size10000 = 10000, count = 0;
	int times[RUNS];
	int n10000[10000];
	
	for(int i = 0; i < size10000; i++)
		n10000[i] = rand() % 20001;
	
	insertion_sort(n10000, size10000);

	while(count < RUNS)
	{
		t1 = clock();
		insertion_sort(n10000, size10000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size10000;
	myfile << "," << average;
}
void unsorted_quick100()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size100 = 100, count = 0;
	int times[RUNS];
	int n100[100];
	
	for(int i = 0; i < size100; i++)
		n100[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n100, 0, size100-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size100; i++)
			n100[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size100;
	myfile << "," << average;
}
void unsorted_quick500()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size500 = 500, count = 0;
	int times[RUNS];
	int n500[500];
	
	for(int i = 0; i < size500; i++)
		n500[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n500, 0, size500-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size500; i++)
			n500[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size500;
	myfile << "," << average;
}
void unsorted_quick1000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size1000 = 1000, count = 0;
	int times[RUNS];
	int n1000[1000];
	
	for(int i = 0; i < size1000; i++)
		n1000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n1000, 0, size1000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size1000; i++)
			n1000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size1000;
	myfile << "," << average;
}
void unsorted_quick2000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size2000 = 2000, count = 0;
	int times[RUNS];
	int n2000[2000];
	
	for(int i = 0; i < size2000; i++)
		n2000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n2000, 0, size2000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size2000; i++)
			n2000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size2000;
	myfile << "," << average;
}
void unsorted_quick5000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size5000 = 5000, count = 0;
	int times[RUNS];
	int n5000[5000];
	
	for(int i = 0; i < size5000; i++)
		n5000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n5000, 0, size5000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size5000; i++)
			n5000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size5000;
	myfile << "," << average;
}
void unsorted_quick10000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size10000 = 10000, count = 0;
	int times[RUNS];
	int n10000[10000];
	
	for(int i = 0; i < size10000; i++)
		n10000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n10000, 0, size10000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size10000; i++)
			n10000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size10000;
	myfile << "," << average;
}
void sorted_quick100()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size100 = 100, count = 0;
	int times[RUNS];
	int n100[100];
	
	for(int i = 0; i < size100; i++)
		n100[i] = rand() % 20001;
	
	quick_sort(n100, 0, size100-1);

	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n100, 0, size100-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size100;
	myfile << "," << average;
}
void sorted_quick500()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size500 = 500, count = 0;
	int times[RUNS];
	int n500[500];
	
	for(int i = 0; i < size500; i++)
		n500[i] = rand() % 20001;
	
	quick_sort(n500, 0, size500-1);

	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n500, 0, size500-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size500;
	myfile << "," << average;
}
void sorted_quick1000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size1000 = 1000, count = 0;
	int times[RUNS];
	int n1000[1000];
	
	for(int i = 0; i < size1000; i++)
		n1000[i] = rand() % 20001;
	
	quick_sort(n1000, 0, size1000-1);

	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n1000, 0, size1000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size1000;
	myfile << "," << average;
}
void sorted_quick2000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size2000 = 2000, count = 0;
	int times[RUNS];
	int n2000[2000];
	
	for(int i = 0; i < size2000; i++)
		n2000[i] = rand() % 20001;
	
	quick_sort(n2000, 0, size2000-1);

	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n2000, 0, size2000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size2000;
	myfile << "," << average;
}
void sorted_quick5000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size5000 = 5000, count = 0;
	int times[RUNS];
	int n5000[5000];
	
	for(int i = 0; i < size5000; i++)
		n5000[i] = rand() % 20001;
	
	quick_sort(n5000, 0, size5000-1);

	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n5000, 0, size5000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size5000;
	myfile << "," << average;
}
void sorted_quick10000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size10000 = 10000, count = 0;
	int times[RUNS];
	int n10000[10000];
	
	for(int i = 0; i < size10000; i++)
		n10000[i] = rand() % 20001;
	
	quick_sort(n10000, 0, size10000-1);

	while(count < RUNS)
	{
		t1 = clock();
		quick_sort(n10000, 0, size10000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size10000;
	myfile << "," << average;
}
void unsorted_heap100()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size100 = 100, count = 0;
	int times[RUNS];
	int n100[100];
	
	for(int i = 0; i < size100; i++)
		n100[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n100, size100);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size100; i++)
			n100[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size100;
	myfile << "," << average;
}
void unsorted_heap500()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size500 = 500, count = 0;
	int times[RUNS];
	int n500[500];
	
	for(int i = 0; i < size500; i++)
		n500[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n500, size500);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size500; i++)
			n500[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size500;
	myfile << "," << average;
}
void unsorted_heap1000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size1000 = 1000, count = 0;
	int times[RUNS];
	int n1000[1000];
	
	for(int i = 0; i < size1000; i++)
		n1000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n1000, size1000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size1000; i++)
			n1000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size1000;
	myfile << "," << average;
}
void unsorted_heap2000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size2000 = 2000, count = 0;
	int times[RUNS];
	int n2000[2000];
	
	for(int i = 0; i < size2000; i++)
		n2000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n2000, size2000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size2000; i++)
			n2000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size2000;
	myfile << "," << average;
}
void unsorted_heap5000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size5000 = 5000, count = 0;
	int times[RUNS];
	int n5000[5000];
	
	for(int i = 0; i < size5000; i++)
		n5000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n5000, size5000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size5000; i++)
			n5000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size5000;
	myfile << "," << average;
}
void unsorted_heap10000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size10000 = 10000, count = 0;
	int times[RUNS];
	int n10000[10000];
	
	for(int i = 0; i < size10000; i++)
		n10000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n10000, size10000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size10000; i++)
			n10000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size10000;
	myfile << "," << average;
}
void sorted_heap100()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size100 = 100, count = 0;
	int times[RUNS];
	int n100[100];
	
	for(int i = 0; i < size100; i++)
		n100[i] = rand() % 20001;
	
	heap_sort(n100, size100);

	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n100, size100);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size100;
	myfile << "," << average;
}
void sorted_heap500()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size500 = 500, count = 0;
	int times[RUNS];
	int n500[500];
	
	for(int i = 0; i < size500; i++)
		n500[i] = rand() % 20001;
	
	heap_sort(n500, size500);

	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n500, size500);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size500;
	myfile << "," << average;
}
void sorted_heap1000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size1000 = 1000, count = 0;
	int times[RUNS];
	int n1000[1000];
	
	for(int i = 0; i < size1000; i++)
		n1000[i] = rand() % 20001;
	
	heap_sort(n1000, size1000);

	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n1000, size1000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size1000;
	myfile << "," << average;
}
void sorted_heap2000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size2000 = 2000, count = 0;
	int times[RUNS];
	int n2000[2000];
	
	for(int i = 0; i < size2000; i++)
		n2000[i] = rand() % 20001;
	
	heap_sort(n2000, size2000);

	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n2000, size2000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size2000;
	myfile << "," << average;
}
void sorted_heap5000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size5000 = 5000, count = 0;
	int times[RUNS];
	int n5000[5000];
	
	for(int i = 0; i < size5000; i++)
		n5000[i] = rand() % 20001;
	
	heap_sort(n5000, size5000);

	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n5000, size5000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size5000;
	myfile << "," << average;
}
void sorted_heap10000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size10000 = 10000, count = 0;
	int times[RUNS];
	int n10000[10000];
	
	for(int i = 0; i < size10000; i++)
		n10000[i] = rand() % 20001;
	
	heap_sort(n10000, size10000);

	while(count < RUNS)
	{
		t1 = clock();
		heap_sort(n10000, size10000);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size10000;
	myfile << "," << average;
}
void unsorted_merge100()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size100 = 100, count = 0;
	int times[RUNS];
	int n100[100];
	int tmp[100];
	
	for(int i = 0; i < size100; i++)
		n100[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n100, tmp, 0, (size100/2), size100-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size100; i++)
			n100[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size100;
	myfile << "," << average;
}
void unsorted_merge500()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size500 = 500, count = 0;
	int times[RUNS];
	int n500[500];
	int tmp[500];
	
	for(int i = 0; i < size500; i++)
		n500[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n500, tmp, 0, (size500/2), size500-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size500; i++)
			n500[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size500;
	myfile << "," << average;
}
void unsorted_merge1000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size1000 = 1000, count = 0;
	int times[RUNS];
	int n1000[1000];
	int tmp[1000];
	
	for(int i = 0; i < size1000; i++)
		n1000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n1000, tmp, 0, (size1000/2), size1000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size1000; i++)
			n1000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size1000;
	myfile << "," << average;
}
void unsorted_merge2000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size2000 = 2000, count = 0;
	int times[RUNS];
	int n2000[2000];
	int tmp[2000];
	
	for(int i = 0; i < size2000; i++)
		n2000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n2000, tmp, 0, (size2000/2), size2000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size2000; i++)
			n2000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size2000;
	myfile << "," << average;
}
void unsorted_merge5000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size5000 = 5000, count = 0;
	int times[RUNS];
	int n5000[5000];
	int tmp[5000];
	
	for(int i = 0; i < size5000; i++)
		n5000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n5000, tmp, 0, (size5000/2), size5000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size5000; i++)
			n5000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size5000;
	myfile << "," << average;
}
void unsorted_merge10000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size10000 = 10000, count = 0;
	int times[RUNS];
	int n10000[10000];
	int tmp[10000];
	
	for(int i = 0; i < size10000; i++)
		n10000[i] = rand() % 20001;
	
	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n10000, tmp, 0, (size10000/2), size10000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size10000; i++)
			n10000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size10000;
	myfile << "," << average;
}
void sorted_merge100()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size100 = 100, count = 0;
	int times[RUNS];
	int n100[100];
	int tmp[100];
	
	for(int i = 0; i < size100; i++)
		n100[i] = rand() % 20001;
	
	merge_sort(n100, tmp, 0, (size100/2), size100-1);

	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n100, tmp, 0, (size100/2), size100-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size100; i++)
			n100[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size100;
	myfile << "," << average;
}
void sorted_merge500()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size500 = 500, count = 0;
	int times[RUNS];
	int n500[500];
	int tmp[500];
	
	for(int i = 0; i < size500; i++)
		n500[i] = rand() % 20001;
	
	merge_sort(n500, tmp, 0, (size500/2), size500-1);

	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n500, tmp, 0, (size500/2), size500-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size500; i++)
			n500[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size500;
	myfile << "," << average;
}
void sorted_merge1000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size1000 = 1000, count = 0;
	int times[RUNS];
	int n1000[1000];
	int tmp[1000];
	
	for(int i = 0; i < size1000; i++)
		n1000[i] = rand() % 20001;
	
	merge_sort(n1000, tmp, 0, (size1000/2), size1000-1);

	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n1000, tmp, 0, (size1000/2), size1000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size1000; i++)
			n1000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size1000;
	myfile << "," << average;
}
void sorted_merge2000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size2000 = 2000, count = 0;
	int times[RUNS];
	int n2000[2000];
	int tmp[2000];
	
	for(int i = 0; i < size2000; i++)
		n2000[i] = rand() % 20001;
	
	merge_sort(n2000, tmp, 0, (size2000/2), size2000-1);

	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n2000, tmp, 0, (size2000/2), size2000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size2000; i++)
			n2000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size2000;
	myfile << "," << average;
}
void sorted_merge5000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size5000 = 5000, count = 0;
	int times[RUNS];
	int n5000[5000];
	int tmp[5000];
	
	for(int i = 0; i < size5000; i++)
		n5000[i] = rand() % 20001;
	
	merge_sort(n5000, tmp, 0, (size5000/2), size5000-1);

	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n5000, tmp, 0, (size5000/2), size5000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size5000; i++)
			n5000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size5000;
	myfile << "," << average;
}
void sorted_merge10000()
{
	srand(time(NULL));
	float t1=0, t2=0, diff=0, milliseconds=0, sum = 0, average=0;
	int size10000 = 10000, count = 0;
	int times[RUNS];
	int n10000[10000];
	int tmp[10000];
	
	for(int i = 0; i < size10000; i++)
		n10000[i] = rand() % 20001;
	
	merge_sort(n10000, tmp, 0, (size10000/2), size10000-1);

	while(count < RUNS)
	{
		t1 = clock();
		merge_sort(n10000, tmp, 0, (size10000/2), size10000-1);
		t2 = clock();
		diff = (t2 - t1);
		milliseconds = diff/(CLOCKS_PER_SEC/1000);
		times[count] = milliseconds;

		for(int i = 0; i < size10000; i++)
			n10000[i] = rand() % 20001;

		count++;
	}
	
	for(int i = 0; i < RUNS; i++)
		sum = sum + times[i];

	average = sum / size10000;
	myfile << "," << average;
}