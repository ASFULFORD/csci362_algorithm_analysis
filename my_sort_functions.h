#ifndef MY_SORT_FUNCTIONS_H_EXISTS
#define MY_SORT_FUNCTIONS_H_EXISTS
void insertion_sort(int *array, int length);
void quick_sort(int *array, int left, int right);
void heap_sort(int *array, int size);
void merge_sort(int *array, int *tmp, int leftPos, int rightPos, int rightEnd);
void percDown(int *array, int i, int n);
int leftChild(int i);
#endif