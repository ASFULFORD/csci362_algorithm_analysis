#include "my_sort_functions.h"
#include <utility>

void insertion_sort(int *array, int length)
{
	int i, j, tmp;
	for(i = 1; i < length; i++)
	{
		j = i;
		while(j > 0 && array[j - 1] > array[j])
		{
			tmp = array[j];
			array[j] = array[j - 1];
			array[j - 1] = tmp;
			j--;
		}
	}
}
void quick_sort(int *array, int left, int right)
{
	int i = left, j = right;
	int tmp;
	int pivot = array[(left + right) / 2];

	while(i <= j)
	{
		while(array[i] < pivot)
			i++;
		while(array[j] > pivot)
			j--;
		if(i <= j)
		{
			tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
			i++;
			j--;
		}
	}

	if(left < j)
		quick_sort(array, left, j);
	if(i < right)
		quick_sort(array, i , right);
}
void heap_sort(int *array, int size)
{
	for(int i = size/(2-1); i >= 0; --i)
	{
		percDown(array, i, size);
	}
	for(int j = size - 1; j > 0; --j)
	{
		std::swap(array[0], array[j]);
		percDown(array, 0, j);
	}
}
void percDown(int *array, int i, int n)
{
	int child;
	int tmp;

	for(tmp = std::move(array[i]); leftChild(i) < n; i = child)
	{
		child = leftChild(i);
		if(child != n - 1 && array[child] < array[child + 1])
			++child;
		if(tmp < array[child])
			array[i] = std::move(array[child]);
		else
			break;
	}
	array[i] = std::move(tmp);
}
int leftChild(int i)
{
	return 2 * i + 1;
}
void merge_sort(int *array, int *tmp, int leftPos, int rightPos, int rightEnd)
{
	int leftEnd = rightPos -1;
	int tmpPos = leftPos;
	int numElements = rightEnd - leftPos + 1;

	while(leftPos <= leftEnd && rightPos <= rightEnd)
	{
		if(array[leftPos] <= array[rightPos])
			tmp[tmpPos++] = std::move(array[leftPos++]);
		else
			tmp[tmpPos++] = std::move(array[rightPos++]);
	}
	while(leftPos <= leftEnd)
		tmp[tmpPos++] = std::move(array[leftPos++]);
	while(rightPos <= rightEnd)
		tmp[tmpPos++] = std::move(array[rightPos++]);
	for(int i = 0; i < numElements; ++i, --rightEnd)
		array[rightEnd] = std::move(tmp[rightEnd]);
}